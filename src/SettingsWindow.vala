/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace GUI {
    public class SettingsWindow : Gtk.Window {

    	private GLib.Settings mySettings;

		private Gtk.Label saveIconDirectoryLabel;
    	private Gtk.Switch saveIconDirectorySwitch;

    	private Gtk.Label saveBinDirectoryLabel;
    	private Gtk.Switch saveBinDirectorySwitch;

		private Gtk.Button acceptButton;
		private Gtk.Button cancelButton;

		public SettingsWindow(Gtk.Window parent){
			this.set_transient_for(parent);
			this.set_modal(true);
			this.set_resizable(false);
			this.set_icon_name("org.vinarisoftware.lanciatore");
			this.set_title(_("Lanciatore - Vinari Software | Preferences"));
		}

    	construct{
    		mySettings=new GLib.Settings("org.vinarisoftware.lanciatore");

    		Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 15);
    		Gtk.Box saveIconBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
    		Gtk.Box saveBinBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
    		Gtk.Box actionButtonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

    		mainBox.set_margin_top(12);
			mainBox.set_margin_bottom(12);
			mainBox.set_margin_start(12);
			mainBox.set_margin_end(12);

			saveIconDirectoryLabel=new Gtk.Label(_("Save the directory that contains the last used icon."));
			saveBinDirectoryLabel=new Gtk.Label(_("Save the directory that contains the last used binary."));

			saveIconDirectorySwitch=new Gtk.Switch();
    		saveBinDirectorySwitch=new Gtk.Switch();

    		saveIconBox.append(saveIconDirectoryLabel);
    		saveIconBox.append(saveIconDirectorySwitch);

    		saveIconDirectoryLabel.set_hexpand(true);
			saveIconDirectoryLabel.set_halign(Gtk.Align.START);
			saveIconDirectorySwitch.set_halign(Gtk.Align.END);

    		saveBinBox.append(saveBinDirectoryLabel);
    		saveBinBox.append(saveBinDirectorySwitch);

    		saveBinDirectoryLabel.set_hexpand(true);
			saveBinDirectoryLabel.set_halign(Gtk.Align.START);
			saveBinDirectorySwitch.set_halign(Gtk.Align.END);

    		cancelButton=new Gtk.Button.with_label(_("Cancel"));
			acceptButton=new Gtk.Button.with_label(_("Apply changes"));

			cancelButton.set_hexpand(true);
			acceptButton.set_hexpand(true);

			actionButtonsBox.append(cancelButton);
			actionButtonsBox.append(acceptButton);

    		Gtk.Separator separatorI=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);

    		mainBox.append(saveBinBox);
    		mainBox.append(saveIconBox);
    		mainBox.append(separatorI);
    		mainBox.append(actionButtonsBox);

			cancelButton.clicked.connect(cancelButtonAction);
			acceptButton.clicked.connect(acceptButtonAction);

			saveBinDirectorySwitch.set_active(mySettings.get_boolean("save-bin-dir"));
			saveIconDirectorySwitch.set_active(mySettings.get_boolean("save-icons-dir"));

    		this.set_child(mainBox);
    	}

    	public void run(){
			this.present();
		}

		private void cancelButtonAction(){
			this.destroy();
		}

		private void acceptButtonAction(){
			this.mySettings.set_boolean("save-icons-dir", saveIconDirectorySwitch.get_active());
			this.mySettings.set_boolean("save-bin-dir", saveBinDirectorySwitch.get_active());

			this.destroy();
		}
    }
}
