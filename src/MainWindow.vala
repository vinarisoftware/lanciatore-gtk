/*
	Copyright (c) 2015 - 2025, Vinari Software
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	   list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	   this list of conditions and the following disclaimer in the documentation
	   and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	   contributors may be used to endorse or promote products derived from
	   this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace GUI {
    [GtkTemplate (ui = "/org/vinarisoftware/lanciatore/MainWindow.ui")]
    public class MainWindow : Gtk.ApplicationWindow {

		[GtkChild] private unowned Gtk.HeaderBar headerBar;
		[GtkChild] private unowned Gtk.Box mainBox;

		private Gtk.Entry appNameEntry;
		private Gtk.Entry appLocationEntry;
		private Gtk.Entry appCommentEntry;
		private Gtk.ComboBoxText selectCategoryCombo;

		private Gtk.Label appNameLabel;
		private Gtk.Label appLocationLabel;
		private Gtk.Label appCommentLabel;
		private Gtk.Label selectIconLabel;
		private Gtk.Label selectCategoryLabel;

		private Gtk.Button saveLauncherButton;
		private Gtk.CheckButton appTerminalCheck;
		private Gtk.Button selectBinaryButton;
		private Gtk.Button selectIconButton;
		private Gtk.Image selectIconImage;

		private Gtk.FileChooserNative iconChooser;
		private Gtk.FileChooserNative binaryChooser;

		private string iconPath;
		private App.Widgets.MessageDialog MsgDialog;
		private GLib.Settings mySettings;

		private string[] categoryTypes={
				"AudioVideo",
				"Audio",
				"Video",
				"Development",
				"Education",
				"Game",
				"Graphics",
				"Network",
				"Office",
				"Science",
				"Settings",
				"System",
				"Utility",
		};

        public MainWindow (Gtk.Application app) {
            Object (application: app);
        }

		construct{
			this.set_title("Lanciatore - Vinari Software");
			this.set_resizable(false);
			mySettings=new GLib.Settings("org.vinarisoftware.lanciatore");

			saveLauncherButton=new Gtk.Button();
			Gtk.Box saveLauncherButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			Gtk.Image saveLauncherButtonImage=new Gtk.Image.from_icon_name("document-save-symbolic");
			Gtk.Label saveLauncherButtonLabel=new Gtk.Label(_("Save launcher"));

			saveLauncherButtonBox.append(saveLauncherButtonImage);
			saveLauncherButtonBox.append(saveLauncherButtonLabel);
			saveLauncherButton.set_child(saveLauncherButtonBox);

			headerBar.pack_start(saveLauncherButton);
			iconPath="applications-system";

			Gtk.Box selectIconBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box secondaryBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			Gtk.Box appNameBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box appLocationBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box appLocationSubBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box appCategoryBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box appCommentBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);

			appNameLabel=new Gtk.Label(_("Name of the application:"));
			appLocationLabel=new Gtk.Label(_("Location of the application's executable:"));
			appCommentLabel=new Gtk.Label(_("Brief comment about the application:"));
			selectIconLabel=new Gtk.Label(_("Select an icon:"));
			selectCategoryLabel=new Gtk.Label(_("Select a category for the application:"));

			selectIconImage=new Gtk.Image.from_icon_name("applications-system");
			selectIconImage.set_pixel_size(48);

			selectIconButton=new Gtk.Button();
			selectIconButton.set_child(selectIconImage);

			Gtk.Box selectBinaryButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Label selectBinaryButtonLabel=new Gtk.Label(_("Browse"));
			Gtk.Image selectBinaryButtonImage=new Gtk.Image.from_icon_name("folder-open-symbolic");

			selectBinaryButtonBox.append(selectBinaryButtonImage);
			selectBinaryButtonBox.append(selectBinaryButtonLabel);

			selectBinaryButton=new Gtk.Button();
			selectBinaryButton.set_child(selectBinaryButtonBox);

			appTerminalCheck=new Gtk.CheckButton.with_label(_("Launch application inside a terminal window"));

			appNameEntry=new Gtk.Entry();
			appLocationEntry=new Gtk.Entry();
			appCommentEntry=new Gtk.Entry();

			selectCategoryCombo=new Gtk.ComboBoxText();
			for(int i=0; i<categoryTypes.length; i++){
				selectCategoryCombo.append_text(categoryTypes[i]);
			}
			selectCategoryCombo.set_active(12);									//Sets the default category as 'Utility'

			selectIconButton.set_vexpand(true);
			appLocationSubBox.set_hexpand(true);
			secondaryBox.set_hexpand(true);
			appLocationEntry.set_hexpand(true);

			selectIconBox.append(selectIconLabel);
			selectIconBox.append(selectIconButton);

			appNameBox.append(appNameLabel);
			appNameBox.append(appNameEntry);

			appLocationSubBox.append(appLocationEntry);
			appLocationSubBox.append(selectBinaryButton);

			appLocationBox.append(appLocationLabel);
			appLocationBox.append(appLocationSubBox);

			appCategoryBox.append(selectCategoryLabel);
			appCategoryBox.append(selectCategoryCombo);

			appCommentBox.append(appCommentLabel);
			appCommentBox.append(appCommentEntry);

			secondaryBox.append(appNameBox);
			secondaryBox.append(appLocationBox);
			secondaryBox.append(appCategoryBox);
			secondaryBox.append(appCommentBox);
			secondaryBox.append(appTerminalCheck);

			selectIconButton.clicked.connect(selectIconButtonAction);
			selectBinaryButton.clicked.connect(selectBinaryButtonAction);
			saveLauncherButton.clicked.connect(saveLauncherButtonAction);

			mainBox.append(selectIconBox);
			mainBox.append(secondaryBox);
		}

		private void iconChooserAction(int response){
			if(response==Gtk.ResponseType.ACCEPT){
				GLib.File iconSelected=this.iconChooser.get_file();
				this.iconPath=iconSelected.get_path();
				this.selectIconImage.set_from_file(this.iconPath);

				if(mySettings.get_boolean("save-icons-dir")==true){
					if(iconSelected.get_parent().get_path()!=mySettings.get_string("icons-dir")){
						mySettings.set_string("icons-dir", iconSelected.get_parent().get_path());
					}
				}
			}else{
				return;
			}
		}

		private void selectIconButtonAction(){
			Gtk.FileFilter iconFilter=new Gtk.FileFilter();
			iconFilter.set_filter_name(_("Image files"));
			iconFilter.add_mime_type("image/*");

			iconChooser=new Gtk.FileChooserNative(_("Select an icon..."), this, Gtk.FileChooserAction.OPEN, _("Select icon"), _("Cancel"));
			iconChooser.set_filter(iconFilter);
			iconChooser.response.connect(iconChooserAction);
			iconChooser.set_modal(true);

			if(mySettings.get_string("icons-dir")!=""){
				GLib.File iconsDir=GLib.File.new_for_path(mySettings.get_string("icons-dir"));
				try{
					iconChooser.set_current_folder(iconsDir);
				}catch(Error ex){
					print("WARNING: %s\n", ex.message);
					mySettings.set_string("bin-dir", "");
				}
			}

			iconChooser.show();
		}

		private void locationChooserAction(int response){
			if(response==Gtk.ResponseType.ACCEPT){
				GLib.File binarySelected=this.binaryChooser.get_file();
				appLocationEntry.set_text(binarySelected.get_path());

				if(mySettings.get_boolean("save-bin-dir")==true){
					if(binarySelected.get_parent().get_path()!=mySettings.get_string("bin-dir")){
						mySettings.set_string("bin-dir", binarySelected.get_parent().get_path());
					}
				}
			}else{
				return;
			}
		}

		private void selectBinaryButtonAction(){
			binaryChooser=new Gtk.FileChooserNative(_("Select an executable..."), this, Gtk.FileChooserAction.OPEN, _("Select app"), _("Cancel"));
			binaryChooser.response.connect(locationChooserAction);
			binaryChooser.set_modal(true);

			if(mySettings.get_string("bin-dir")!=""){
				GLib.File binDir=GLib.File.new_for_path(mySettings.get_string("bin-dir"));
				try{
					binaryChooser.set_current_folder(binDir);
				}catch(Error ex){
					print("WARNING: %s\n", ex.message);
					mySettings.set_string("bin-dir", "");
				}
			}

			binaryChooser.show();
		}

		private void newLauncher(){
			appNameEntry.set_text("");
			appCommentEntry.set_text("");
			appLocationEntry.set_text("");
			this.iconPath="applications-system";
			this.selectIconImage.set_from_icon_name("applications-system");
			appTerminalCheck.set_active(false);
		}

		private void saveLauncherButtonAction(){
			if(appNameEntry.get_text()=="" || appLocationEntry.get_text()==""){
				MsgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, (_("An application name and location are necesary to create a launcher.\nPlease, fix this and try again.")));
				return;
			}

			WriteLauncher writingInstance=new WriteLauncher();
			string categoryTemp=categoryTypes[selectCategoryCombo.get_active()];
			bool isWritten=writingInstance.generateLauncher(appNameEntry.get_text(), appCommentEntry.get_text(), appLocationEntry.get_text(), this.iconPath, appTerminalCheck.get_active(), categoryTemp);

			if(isWritten){
				generateNotification("org.vinarisoftware.lanciatore", _("The launcher for ")+appNameEntry.get_text()+_(" has been created for this user."));
				newLauncher();
			}else{
				MsgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, (_("There was an unexpected error while writing the launcher...\nDo you have enough permissions?")));
				return;
			}
		}

		private void generateNotification(string notifID, string notifMessage){
			Gtk.Application? app=get_application();
			Notification notification = new Notification ("Lanciatore - Vinari Software");
			notification.set_priority(HIGH);
			notification.set_title(_("Launcher successfully created!"));
			notification.set_body(notifMessage);
			app.send_notification (notifID, notification);
		}
    }
}
